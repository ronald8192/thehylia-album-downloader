# The Hylia Album Downloader

Album Downloader for anime.thehylia.com

## Download and Install dependencies
    git clone --depth 1 https://bitbucket.org/ronald8192/thehylia-album-downloader.git
    cd thehylia-album-downloader
    npm install

## How to use

```
npm start {url} {download-to-folder}
```
 
## Tested in

    node 6.11.2
    npm  3.10.10

