"use strict";

const url = process.argv[2];
if (!url) throw new Error("Please provide url: `npm start https://... <download to folder>`");
let downloadTo = process.argv[3];
if (!downloadTo) throw new Error("Please provide url: `npm start https://... <download to folder>`");
if (!downloadTo.endsWith("/")) downloadTo += "/";

const request = require("request");
const cheerio = require("cheerio");
const fs = require("fs");

let downloadDir = downloadTo + url.split("/").pop();
let download = (link, headers) => {
	if(!fs.existsSync(downloadDir)) {
		if(downloadDir.startsWith("/")){
			mkdirp("/", downloadTo);
		} else {
			mkdirp(process.cwd(), downloadTo);
		}
	}

	if (!fs.existsSync(downloadDir)) { //create dir if not exist
		console.log(`create dir "${downloadDir}"`);
		fs.mkdir(downloadDir);
	}

	let filename = decodeURI(link.split("/").pop());
	let downloadPath = `${downloadDir}/${filename}`;
	console.log(`downloadPath: ${downloadPath}`);
	if(fs.existsSync(downloadPath)){ //delete file if exist
		console.log(`Delete ${downloadPath}`)
		fs.unlinkSync(downloadPath);
	}
	console.log(`Download ${filename}`);
	request({url:link, header:headers}).pipe(fs.createWriteStream(downloadPath));
}

let openDownloadPage = (link) => {
	request({
		url: link,
		header: {
			"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
			"Accept-Encoding":"gzip, deflate",
			"Accept-Language":"en-US,en;q=0.9",
			"Connection":"keep-alive",
			"Host":link.replace(/(https|http):\/\//,"").split("/")[0],
			"Upgrade-Insecure-Requests":1,
			"User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36",
		}
	}, (error, response, body) => {
		let $ = cheerio.load(body);
		let songlocation = $("#content_container > div.floatbox > table > tbody > tr > td > div > p:nth-child(6) > b:nth-child(6) > a")
						.attr("href");
		download(songlocation,{
			"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
			"Accept-Encoding":"gzip, deflate",
			"Accept-Language":"en-US,en;q=0.9",
			"Connection":"keep-alive",
			"Host":songlocation.replace(/(https|http):\/\//,"").split("/")[0],
			"Upgrade-Insecure-Requests":1,
			"User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36",
		});
	});
}

let mkdirp = (parent, path) => {
	let dirs = (path instanceof Array) ? path : path.split("/");
	let newDir = `${parent}/${dirs[0]}`;
	if (!fs.existsSync(newDir)) {
		console.log(`create ${newDir}`);
		fs.mkdir(newDir);
	}else{
		console.log(`dir exist ${newDir}`);
	}
	if(dirs.length > 1) {
		dirs.shift()
		mkdirp(newDir, dirs);
	}
}

request({
	url: url,
	header:{
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
		"Accept-Encoding":"gzip, deflate, br",
		"Accept-Language":"en-US,en;q=0.9",
		"Connection":"keep-alive",
		"Host":"anime.thehylia.com",
		"Referer":"https://anime.thehylia.com/soundtracks/browse/all",
		"Upgrade-Insecure-Requests":1,
		"User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36"
	}
}, (error, response, body) => {
	// console.log('error:', error);
	// console.log('statusCode:', response && response.statusCode);
	// console.log('body:', body);

  	//song
	let $ = cheerio.load(body);
	let songLinks = $("#content_container > div.floatbox > table > tbody > tr > td > div > table > tbody")
					.find("a");
	console.log(`${songLinks.length} songs`);
	for (var i = 0; i < songLinks.length; i++) {
		openDownloadPage(songLinks[""+i].attribs.href);
	}

	// album art
	let albumArtLinks = $("#content_container > div.floatbox > table > tbody > tr > td > div")
						.find("img");
	console.log(`${albumArtLinks.length} album art`);
	for (var i = 0; i < albumArtLinks.length; i++) {
		let imgUrl = albumArtLinks[""+i].parent.attribs.href
		download(imgUrl, {
			"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
			"Accept-Encoding":"gzip, deflate",
			"Accept-Language":"en-US,en;q=0.9",
			"Connection":"keep-alive",
			"Host":imgUrl.replace(/(https|http):\/\//,"").split("/")[0],
			"Upgrade-Insecure-Requests":1,
			"User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36",
		});
	}
});

